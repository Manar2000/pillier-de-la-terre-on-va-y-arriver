package model;

import javafx.scene.image.Image;
import model.ressources.Ressource;

import java.util.HashMap;
import java.util.Map;

public class CarteArtisan extends Carte {
    private Map<Ressource,Integer> coutEnRessources;
    private int points;
    private int cout;
    private boolean stonecutter=false;
    private boolean dayLaborer=false;


    public CarteArtisan(Image img,HashMap<Ressource, Integer> ressources, int points, int cout, boolean s, boolean dl) {
        super(img);
        this.coutEnRessources = ressources;
        this.points = points;
        this.cout = cout;
        stonecutter=s;
        dayLaborer=dl;
    }

    public int getCout() {
        return cout;
    }

    public boolean jouerCarte(Player player){
        if (dayLaborer){
//a completer
        }
        else {
            // si l'artisan n'a pas besoin de ressource on gagne les points directement
            if (coutEnRessources.size()==0){
                player.setNumberVictoryPoints(player.getNumberVictoryPoints()+points);
                return true;
            }
            for(Map.Entry<Ressource, Integer> entry : coutEnRessources.entrySet()) {
                Ressource ressource = entry.getKey();
                Integer valeur = entry.getValue();
                System.out.print("ressource"+ressource+"cout"+entry.getValue());
                if (player.getInventory().get(ressource)>=valeur){
                    player.getInventory().replace(ressource,player.getInventory().get(ressource)-valeur);
                }

                else return false;
            }
            player.setNumberVictoryPoints(player.getNumberVictoryPoints()+points);
            System.out.println("player "+player.getColor()+"wins"+points);
        }
        return true;
    }
    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


}
