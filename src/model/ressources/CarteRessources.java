package model.ressources;

import javafx.scene.image.Image;
import model.Carte;
import model.Player;

public class CarteRessources extends Carte {
    private int ouvriers;
    private int nbRessourcesGagnees;
    private Ressource type;

    public CarteRessources(Image img, int ouvriers, int nbRessourcesGagnees, Ressource type) {
        super(img);
        this.ouvriers = ouvriers;
        this.nbRessourcesGagnees = nbRessourcesGagnees;
        this.type = type;
    }

    public int getOuvriers() {
        return ouvriers;
    }

    public int getNbRessourcesGagnees() {
        return nbRessourcesGagnees;
    }

    public Ressource getType() {
        return type;
    }

    public boolean play(Player p){
        if (p.getInventory().get(Ressource.SMALLWORKER)>= ouvriers){
            p.getInventory().replace(type,p.getInventory().get(type)+nbRessourcesGagnees);
        }
        return false;
    }
}
