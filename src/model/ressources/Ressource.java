package model.ressources;

public enum Ressource {
    BOIS(3, true, "Bois"), SABLE(2,true, "Sable"),
    PIERRE(4,true, "Pierre"), METAL(10,true, "Métal"),
    GOLD(1, false, "Or"),MASTERBUILDERNOIR(1, false, "Bâtisseur"),
    MASTERBUILDER(1, false,""),SMALLWORKER(1, false,"Ouvrier");

    Ressource(int value, boolean isExcheangeable, String name) {
        this.value = value;
        this.isExcheangeable = isExcheangeable;
        this.name=name;
    }

    public int exchange(int quantity){
        return quantity*value;
    }

    public int getValue() {
        return value;
    }

    private int value;
    private boolean isExcheangeable;
    private String name;

    public boolean isExcheangeable() {
        return isExcheangeable;
    }

    public String getName() {
        return name;
    }
}