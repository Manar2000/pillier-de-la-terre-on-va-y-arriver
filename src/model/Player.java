package model;

import exception.NegativeQuantityException;
import model.ressources.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Player {

    private HashMap<BonusMalus, Boolean>  bonus;
    private PlayersColors color;
    private int numberVictoryPoints;
    private HashMap<Ressource,Integer> inventory;
    private HashMap<Privilege,Boolean> privileges;
    private ArrayList<CarteArtisan> cartes;
    private ArrayList<CarteRessources> cartesRessources;

    public ArrayList<CarteArtisan> getCartes() {
        return cartes;
    }

    public void setCartes(ArrayList<CarteArtisan> cartes) {
        this.cartes = cartes;
    }

    public ArrayList<CarteRessources> getCartesRessources() {
        return cartesRessources;
    }

    public Player(PlayersColors color) {

        this.color = color;
        this.numberVictoryPoints = 0;
        bonus = new HashMap<>();
        inventory = new HashMap<>();
        setupInventory();
        privileges = new HashMap<>();
        cartes=new ArrayList<>();
        cartesRessources=new ArrayList<>();
        setupPrivileges();
        refreshBonus();
    }
    public void refreshBonus(){
        for (BonusMalus b: BonusMalus.values()){
            bonus.put(b,false);
        }
    }

    public void setupInventory() {
        inventory.put(Ressource.GOLD,0);
        inventory.put(Ressource.BOIS,0);
        inventory.put(Ressource.METAL,0);
        inventory.put(Ressource.PIERRE,0);
        inventory.put(Ressource.SABLE,0);
        inventory.put(Ressource.MASTERBUILDER,3);
        inventory.put(Ressource.SMALLWORKER,12);
    }
    public void setupPrivileges() {
        for (Privilege p:Privilege.values()) {
            privileges.put(p,false);
        }
    }


    /**
     * Exchange resources and add the points to numberVictoryPoints
     */

    public boolean pay(int value, boolean forcePay){
        int initGold=inventory.get(Ressource.GOLD);
        if(value<=initGold){
            inventory.put(Ressource.GOLD, initGold-value);
            System.out.println(1);
            return true;
        }
        else if(forcePay) {
            System.out.println(2);
            inventory.put(Ressource.GOLD, 0);
            return true;
        }
        return false;
    }
    public HashMap<Ressource, Integer> getInventory() {
        return inventory;
    }

    public void addToInventory(Ressource r, Integer q) throws exception.NegativeQuantityException{

        if(q<0) throw new NegativeQuantityException();
        this.inventory.replace(r,inventory.get(r)+q);
    }

    public int getNumberVictoryPoints() {
        return numberVictoryPoints;
    }

    public PlayersColors getColor() {
        return color;
    }

    public void addToNumberVictory(int points){
        numberVictoryPoints+=points;
    }

    @Override
    public String toString() {

        return "Joueur " + this.getColor().getName() +
                "\nPoints:" + numberVictoryPoints +
                "\n Ressources:" +
                "\n Métal:" + inventory.get(Ressource.METAL) +
                " Pièrre:" + inventory.get(Ressource.PIERRE) +
                "\n Bois:" + inventory.get(Ressource.BOIS) +
                " Sable:" + inventory.get(Ressource.SABLE) ;
    }

    public void addToVictoryPoints(int d) {
        this.numberVictoryPoints += d;
    }

    public void setNumberVictoryPoints(int numberVictoryPoints) {
        this.numberVictoryPoints = numberVictoryPoints;
    }

    public void setInventory(HashMap<Ressource, Integer> inventory) {
        this.inventory = inventory;
    }
    public HashMap<Privilege, Boolean> getPrivileges() {
        return privileges;
    }

    public HashMap<BonusMalus, Boolean> getBonus() {
        return bonus;
    }

    public void setCartesRessources(ArrayList<CarteRessources> cartesRessources) {
        this.cartesRessources = cartesRessources;
    }

    public void setBonus(HashMap<BonusMalus, Boolean> bonus) {
        this.bonus = bonus;
    }

    public void setPrivileges(HashMap<Privilege, Boolean> privileges) {
        this.privileges = privileges;
    }
}
