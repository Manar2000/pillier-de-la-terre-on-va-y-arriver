package model;

public class Map<B, B1> {
    private PawsZone zoneSable;
    private PawsZone zoneBois;
    private PawsZone courRoi;
    private PawsZone zonePierre;
    private PawsZone zoneEvache;

    public PawsZone getZoneEvache() {
        return zoneEvache;
    }

    public Map(){
        zoneSable=new PawsZone("zone sable");
        zoneBois=new PawsZone("zone bois");
        zonePierre=new PawsZone("zone pierre");
        courRoi=new PawsZone("cour du roi");
        zoneEvache=new PawsZone("évéché");
    }

    public PawsZone getZoneSable() {
        return zoneSable;
    }

    public void setZoneSable(PawsZone zoneSable) {
        this.zoneSable = zoneSable;
    }

    public PawsZone getZoneBois() {
        return zoneBois;
    }

    public void setZoneBois(PawsZone zoneBois) {
        this.zoneBois = zoneBois;
    }

    public PawsZone getCourRoi() {
        return courRoi;
    }

    public void setCourRoi(PawsZone courRoi) {
        this.courRoi = courRoi;
    }

    public PawsZone getZonePierre() {
        return zonePierre;
    }

    public void setZonePierre(PawsZone zonePierre) {
        this.zonePierre = zonePierre;
    }
}
