package model;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.ressources.CarteRessources;
import model.ressources.Ressource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static model.BonusMalus.*;

public class Game {
    private int indexCurrentPlayer;
    private ArrayList<Player> lPlayers;
    private Map map;
    private int nbToursRestants;
    private ArrayList<ZonesBatisseurs> lZonesBatisseurs;
    private HashMap<ImageView, CarteArtisan> cartesArtisans;
    private HashMap<ImageView, CarteRessources> cartesRessources;
    private ArrayList<CarteEvenement> evenements;
    private boolean taxes;
    private boolean market;

    public HashMap<ImageView, CarteRessources> getCartesRessources() {
        return cartesRessources;
    }

    public boolean isTaxes() {
        return taxes;
    }

    public void setTaxes(boolean taxes) {
        this.taxes = taxes;
    }

    public boolean isMarket() {
        return market;
    }

    public void setMarket(boolean market) {
        this.market = market;
    }



    public Game(ArrayList players){
        lPlayers=players;
        map=new Map();
        nbToursRestants=1;
        market=false;
        taxes=false;
        evenements= new ArrayList<CarteEvenement>();
    }

    public ArrayList<CarteEvenement> getEvenements() {
        return evenements;
    }

    public void initEvenements(CarteEvenement[] values, ArrayList<CarteEvenement> evenements) {
        for (CarteEvenement c : values){
            evenements.add(c);
        }
    }
    public void initCartesRessources() {
        cartesRessources = new HashMap<>();
        CarteRessources carte;
        carte = new CarteRessources(new Image("img/ressources/carteRessources1.png"),2,2,Ressource.SABLE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);
        carte = new CarteRessources(new Image("img/ressources/carteRessources2.png"),4,3,Ressource.SABLE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources3.png"),7,4,Ressource.SABLE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources4.png"),5,2,Ressource.PIERRE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources5.png"),6,3,Ressource.PIERRE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources6.png"),10,4,Ressource.PIERRE);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources7.png"),3,2,Ressource.BOIS);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources8.png"),6,3,Ressource.BOIS);
        cartesRessources.put(new ImageView(carte.getImage()),carte);

        carte = new CarteRessources(new Image("img/ressources/carteRessources9.png"),9,4,Ressource.BOIS);
        cartesRessources.put(new ImageView(carte.getImage()),carte);
    }
        public void initCartesArtisan() {
        cartesArtisans = new HashMap<>();
        HashMap<Ressource,Integer> ressource = new HashMap<>();
        ressource.put(Ressource.PIERRE,1);
        ressource.put(Ressource.SABLE,1);
        ressource.put(Ressource.BOIS,1);
        ImageView[] images = new ImageView[9];
        for (int i=0;i<9;i++) {
            images[i] = new ImageView();
            images[i].setImage(new Image("img/artisans/artisan"+(i+1)+".png"));
            cartesArtisans.put(images[i],null);

        }

        cartesArtisans.put(images[0],new CarteArtisan(images[0].getImage(),ressource,5,5,false,false));
        ressource.clear();
        cartesArtisans.put(images[2],new CarteArtisan(images[2].getImage(),ressource,1,3,false,false));
        ressource.clear();
        ressource.put(Ressource.METAL,1);
        cartesArtisans.put(images[3],new CarteArtisan(images[3].getImage(),ressource,2,2,false,false));
        ressource.clear();
        ressource.put(Ressource.BOIS,1);
        ressource.put(Ressource.SABLE,1);
        ressource.put(Ressource.PIERRE,1);
        cartesArtisans.put(images[4],new CarteArtisan(images[4].getImage(),ressource,1,1,false,false));
        ressource.clear();
        cartesArtisans.put(images[8],new CarteArtisan(images[8].getImage(),ressource,2,5,false,false));
        ressource.put(Ressource.SABLE,1);
        cartesArtisans.put(images[7],new CarteArtisan(images[7].getImage(),ressource,2,6,false,false));
        ressource.clear();
        ressource.put(Ressource.SABLE,1);
        ressource.put(Ressource.BOIS,1);
        ressource.put(Ressource.PIERRE,1);
        cartesArtisans.put(images[4],new CarteArtisan(images[4].getImage(),ressource,1,1,false,true));
        ressource.clear();
        ressource.put(Ressource.PIERRE,1);
        cartesArtisans.put(images[6],new CarteArtisan(images[6].getImage(),ressource,5,1,true,false));
        cartesArtisans.put(images[1],new CarteArtisan(images[1].getImage(),ressource,1,4,true,false));
        cartesArtisans.put(images[5],new CarteArtisan(images[5].getImage(),ressource,0,0,true,false));


    }
    public HashMap<ImageView, CarteArtisan> getCartesArtisans() {
        return cartesArtisans;
    }



    public void initZonesBatisseurs(){
        lZonesBatisseurs = new ArrayList<>();
        lZonesBatisseurs.addAll(Arrays.asList(ZonesBatisseurs.values()));
    }
    public ArrayList<Player> getlPlayers() {
        return lPlayers;
    }

    public void refreshZonesBatisseurs(ArrayList<ZonesBatisseurs> lZonesBatisseurs){
        for(int i=0; i<lZonesBatisseurs.size(); i++){
            System.out.println("a");
            lZonesBatisseurs.get(i).getlBatisseurs().clear();
        }
    }

    boolean addBatisseurToZone(PlayersColors batisseurColor, int numZone){
        if(numZone<1 || numZone>lZonesBatisseurs.size())
            return false;
        return lZonesBatisseurs.get(numZone-1).addBatisseur(batisseurColor);
    }

    public Player whoWon(ArrayList<Player> lPlayers) {
        int max= lPlayers.get(0).getNumberVictoryPoints();
        Player winner= lPlayers.get(0);
        for(Player p: lPlayers) {
            if(p.getNumberVictoryPoints()>max) {
                max=p.getNumberVictoryPoints();
                winner=p;
            }
        }
        return winner;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    public int getNbToursRestants() {
        return nbToursRestants;
    }

    public void setNbToursRestants(int nbToursRestants) {
        this.nbToursRestants = nbToursRestants;
    }

    public int getIndexCurrentPlayer() {
        return indexCurrentPlayer;
    }

    public void setIndexCurrentPlayer(int indexCurrentPlayer) {
        this.indexCurrentPlayer = indexCurrentPlayer;
    }
    public boolean toNextPlayer(boolean autoNextTurn){//retourn true si on passe au tour suivant
        if(indexCurrentPlayer +1<lPlayers.size()){
            indexCurrentPlayer +=1;
            return false;
        }
        else{
            if(autoNextTurn)
                toNextTurn(lPlayers, true);
            return true;
        }
    }

    public void toNextTurn(ArrayList<Player> lPlayers, boolean activeGraphics) {
        nbToursRestants-=1;
        //on change de premier joueur
        Player pCopy = lPlayers.remove(0);
        lPlayers.add(pCopy);
        //l'index du joueur qui joue passe à 1
        indexCurrentPlayer=0;
        //on fait recup les ressources aux joueurs
        recupRessources(map.getZoneEvache(), Ressource.GOLD);
        //recup ressources des cartes
        int perte =0;
        for (Player p : lPlayers){
            if (p.getBonus().get(BonusMalus.PERTE_UNE_RESSOURCE_PAR_CARTE)){
                p.getBonus().replace(PERTE_UNE_RESSOURCE_PAR_CARTE,false);
                perte=1;
            }
            if (p.getBonus().get(BonusMalus.RESSOURCE_EN_PLUS_PAR_CARTE)){
                p.getBonus().replace(RESSOURCE_EN_PLUS_PAR_CARTE,false);
                perte=-1;
            }

            for (CarteRessources carteR : p.getCartesRessources()){
                if(carteR.getNbRessourcesGagnees()-perte>0)
                    p.getInventory().replace(carteR.getType(),
                            p.getInventory().get(carteR.getType())+carteR.getNbRessourcesGagnees()-perte);

            }
        }

        for (Player p: this.lPlayers){
            if (p.getBonus().get(BonusMalus.DEUX_BATISSEURS_TOUR_SUIVANT)){
                p.getBonus().replace(DEUX_BATISSEURS_TOUR_SUIVANT,false);
                p.getInventory().replace(Ressource.MASTERBUILDER,5);
            }
            else p.getInventory().replace(Ressource.MASTERBUILDER,3);
            p.getInventory().replace(Ressource.SMALLWORKER,12);
            p.setCartesRessources(new ArrayList<>());
            p.getBonus().replace(PROTEGE_CONTRE_EVENEMENT,false);

        }
        if(activeGraphics) {
            initCartesRessources();
            refreshZonesBatisseurs(lZonesBatisseurs);
        }
        map = new Map();
    }
    private void recupRessources(PawsZone pz, Ressource ressource){
        for(PlayersColors c:PlayersColors.values()){
            int n = pz.getNbPawsPerColor().get(c);
            for(Player p: lPlayers){
                if(p.getColor()==c) {
                    int nbInit=p.getInventory().get(ressource);
                    p.getInventory().put(ressource, nbInit+n);

                }
            }
            pz.getNbPawsPerColor().put(c,0);

        }

    }
    public void exchangeAllRessources(ArrayList<Player> lPlayers){

        for(Player p: lPlayers){
            for (CarteArtisan c : p.getCartes()){
                //c.jouerCarte(p);
            }
            for (Ressource r: Ressource.values()){
                if(r.equals(Ressource.METAL) || r.equals(Ressource.PIERRE)
                || r.equals(Ressource.BOIS) || r.equals(Ressource.SABLE)){
                    System.out.println("okd"+p.getNumberVictoryPoints());
                    p.setNumberVictoryPoints(p.getNumberVictoryPoints()+p.getInventory().get(r)*r.getValue());
                    p.getInventory().replace(r,0);
                }

            }

        }

    }
    public Player getCurrentPlayer(){
        return lPlayers.get(indexCurrentPlayer);
    }
    /*
    methode pour prelever obtenir la taxe du roi
     */
    public int getKingTax(){
        int taxeValue= (int) (Math.random()*6+1);
        switch (taxeValue){
            case 1: taxeValue=2;break;
            case 2:
            case 3:taxeValue = 3; break;
            case 4:
            case 5:taxeValue=4; break;
            default:taxeValue=5;break;
        }
        return taxeValue;
    }
    /*
    methode pour appliquer la taxe du roi
    @param lPlayer: la liste des joueurs du jeu
    @param LPlayerInKingCourt: liste des joueurs dont un batisseur est dans la cour du roi
     */
    public void applyKingTax(ArrayList<Player> lPlayers ,ArrayList<Player> lPlayerInKingCourt, int taxValue){
        for(Player p:lPlayers){
            if(!lPlayerInKingCourt.contains(p)){
                p.pay(taxValue, true);
            }
        }
    }
    public ArrayList<ZonesBatisseurs> getlZonesBatisseurs() {
        return lZonesBatisseurs;
    }

}
