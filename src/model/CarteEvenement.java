package model;

import model.ressources.Ressource;

import java.util.ArrayList;

public enum CarteEvenement {
    REVENUS_DOUBLES_MANUFACTURE("Revenus doublés à la manufacture de la laine", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers){
                p.getBonus().put(BonusMalus.REVENUS_MANUFACTURE_DOUBLES, true);
            }
        }
    }), GAIN_5_OR("Tous les joueurs gagnent 5 d'or", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers){
                int n=p.getInventory().get(Ressource.GOLD);
                p.getInventory().put(Ressource.GOLD, n+5);
            }
        }
    }),
    AJOUT_UNE_CAPACITE_ARTISANT("Tous les artisants peuvent effectuer une transformation une fois de plus", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers){
                p.getBonus().put(BonusMalus.AJOUT_1_CAPACITE_ARTISANTS, true);
            }
        }
    }),
    RESSOURCE_EN_PLUS_PAR_CARTE("Toutes les cartes ressources utilisées rapportent une ressource en plus", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers){
                p.getBonus().put(BonusMalus.RESSOURCE_EN_PLUS_PAR_CARTE, true);
            }
        }
    }),
    GAIN_1_METAL("Tous les joueurs gagnent 1 métal", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers){
                int n=p.getInventory().get(Ressource.METAL);
                p.getInventory().put(Ressource.GOLD, n+1);
            }
        }
    }),
    DEUX_BATISSEURS_TOUR_SUIVANT("Tous les joueurs n'étant pas protégés à l'Evéché auront deux bâtisseurs au tour suivant", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers) {
                if (!p.getBonus().get(BonusMalus.PROTEGE_CONTRE_EVENEMENT)) {
                    p.getBonus().put(BonusMalus.DEUX_BATISSEURS_TOUR_SUIVANT, true);
                }
            }
        }
    }),
    PERTE_UNE_RESSOURCE_PAR_CARTE("Tous les joueurs n'étant pas protégés à l'Evéché gagneront une ressource " +
            "de moins par carte ressource utilisée", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers) {
                if (!p.getBonus().get(BonusMalus.PROTEGE_CONTRE_EVENEMENT)) {
                    p.getBonus().put(BonusMalus.PERTE_UNE_RESSOURCE_PAR_CARTE, true);
                }
            }
        }
    }),
    PERTE_4_OR("Tous les joueurs n'étant pas protégés à l'Evéché perdent 4 d'or", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers) {
                if (!p.getBonus().get(BonusMalus.PROTEGE_CONTRE_EVENEMENT)) {
                    int n = p.getInventory().get(Ressource.GOLD);
                    if(n<4)
                        p.getInventory().put(Ressource.GOLD, 0);
                    else
                        p.getInventory().put(Ressource.GOLD, n-4);
                }
            }
        }
    }),

    PERTE_2_PV("Tous les joueurs n'étant pas protégés à l'Evéché perdent 2 points de victoire", new ActionInterface() {
        @Override
        public void play(ArrayList<Player> lPlayers) {
            for(Player p:lPlayers) {
                if (!p.getBonus().get(BonusMalus.PROTEGE_CONTRE_EVENEMENT)) {
                    p.addToNumberVictory(-2);
                    if(p.getNumberVictoryPoints()<0)
                        p.setNumberVictoryPoints(0);
                }
            }
        }
    });

    public String getActionName() {
        return actionName;
    }

    private String actionName;
    private ActionInterface action;
    CarteEvenement(String actionName, ActionInterface action){
        this.actionName=actionName;
        this.action=action;
    }
    public void play(ArrayList<Player> lPlayers){
        action.play(lPlayers);
    }
}
