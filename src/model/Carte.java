package model;

import javafx.scene.image.Image;

public class Carte {
    protected Image image;

    public Carte(Image img){
        image=img;
    }

    public Image getImage() {
        return image;
    }
}
