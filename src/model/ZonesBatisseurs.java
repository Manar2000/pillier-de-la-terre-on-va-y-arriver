package model;

import java.util.ArrayList;

public enum ZonesBatisseurs {
    EVECHE("Eveché", 1, 1),
    FILATURE_LAINE("Filature de la laine", 1, -1),
    KINGSBRIDE("Kingsbride", 4, 2),
    PRIEURE("Prieuré de KingsBridge", 2, 3),
    FORET("Forêt",0,-1),
    CARRIERE("Carrière",0,-1),
    SABLIERE("Sabliere",0,-1),
    COUR_DU_ROI("Cour du roi", 4, 4),
    SHIRING("Shiring", 2, 5),
    CHATEAU_DE_SHIRING("Chateau de Shiring", 1, 6),
    MARCHE("Marché", 12, 7),
    CATHEDRALE("Cathédrale", 1, 8);

    private String name;
    private ArrayList<PlayersColors> lBatisseurs;
    private int limit;
    private int indexBatisseur; //-1 si on peut pas en poser sur la zone
    ZonesBatisseurs(String name, int limit, int indexBatisseur){
        this.name=name;
        this.limit=limit;
        this.indexBatisseur=indexBatisseur;
        lBatisseurs =new ArrayList<>();
    }
    public boolean addBatisseur(PlayersColors pc){
        if(lBatisseurs.size()>=limit)
            return false;
        lBatisseurs.add(pc);
        return true;
    }
    public static ZonesBatisseurs find(int index){
        for(ZonesBatisseurs z : values()){
            if(z.indexBatisseur==index)
                return z;
        }
        return null;
    }
    public ArrayList<PlayersColors> getlBatisseurs() {
        return lBatisseurs;
    }
}
