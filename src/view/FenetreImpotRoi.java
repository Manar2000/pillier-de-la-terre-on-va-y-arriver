package view;

import model.Player;

import java.io.IOException;
import java.util.ArrayList;

public class FenetreImpotRoi extends FenetreDialogue{
    public FenetreImpotRoi(int taxValue, ArrayList<Player> lTaxedPlayers, ArrayList<Player> lPlayerInKingCourt)  {
        super("Les pilliers de la Terre","","", true);
        setFenTitle("");
        setTitle("Les pilliers de la Terre");
        String content = "Le roi prend son impot";
        content+=",";
        if(lTaxedPlayers.size()!=0) {
            for (Player p : lTaxedPlayers) {
                content += " joueur " + p.getColor().getName() + ", ";
            }
            content += " vous payez " + taxValue + " au roi.\n";
        }
        if(lPlayerInKingCourt.size()!=0) {
            content += "Joueur ";
            for (Player p : lPlayerInKingCourt) {
                content += p.getColor().getName() + ", ";
            }
            content+= " vous êtes exonéré/s d'impot car un vos/votre bâtisseurs est présent dans la cour du roi.";
        }
        setContent(content);
        setPeoples("tous les joueurs");
    }

    @Override
    public void loadForm() {

    }
}
