package view;

import controller.BuilderPlaceController;
import controller.GameController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Carte;
import model.Player;
import model.ressources.CarteRessources;
import model.ressources.Ressource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

public class FenetreMesCartes extends Stage {
    private Player player;
    Pane root;
    private Rectangle2D primaryScreenBounds;
    private Text[] texts;
    private Text[] playersInfo;
    private ArrayList<Player> lplayers;
    private String[] s;
    private AnchorPane anchorPane;
    private  GridPane grid;


    public FenetreMesCartes(Player player, Rectangle2D primaryScreenBounds) throws IOException {
        super();
        if (player!=null){
            this.player=player;
            this.primaryScreenBounds=primaryScreenBounds;
            root = FXMLLoader.load(getClass().getResource("fxml/gameScene.fxml"));
            root.getStylesheets().add(getClass().getResource("css/carteScene.css").toExternalForm());
            setScene(new Scene(root, primaryScreenBounds.getWidth(), primaryScreenBounds.getHeight()- primaryScreenBounds.getHeight()/30));
            setResizable(false);
            loadCartes();
            show();
        }



    }
    public void loadCartes(){
        AnchorPane anchorPane = (AnchorPane) root.lookup("#anchor");
        ArrayList<ImageView> artisans = new ArrayList<>();
        int m=0;
        ImageView imgvide = new ImageView(new Image("img/vide.png"));
        imgvide.setFitWidth(primaryScreenBounds.getWidth()/6.79);imgvide.setFitHeight(primaryScreenBounds.getHeight()/2.15);
        imgvide.setOpacity(1);
        ArrayList<ImageView> images = new ArrayList<>();
        for ( Carte key : player.getCartes()) {
            ImageView img = new ImageView(key.getImage());
            img.setFitWidth(primaryScreenBounds.getWidth()/6.79);img.setFitHeight(primaryScreenBounds.getHeight()/2.15);
            img.setOpacity(1);
            images.add(img);
        }
        for (CarteRessources c : player.getCartesRessources()){
            ImageView img = new ImageView(c.getImage());
            img.setFitWidth(primaryScreenBounds.getWidth()/6.79);img.setFitHeight(primaryScreenBounds.getHeight()/2.15);
            img.setOpacity(1);
            images.add(img);

        }
        anchorPane.setStyle("-fx-background-image: url('img/vide.png')");
        GridPane grid= new GridPane();

        grid.setMinWidth(0);grid.setMinHeight(0);

        grid.setLayoutX(0);
        grid.setLayoutY(0);
        grid.setStyle("-fx-background-color: rgba(255,255,255,0.4);");
        int i=0;
        int k=0;

        for (ImageView img : images){
            grid.add(img,i,k);
            i++;
            if (i==5) {k++;i=0;}
        }

        anchorPane.getChildren().add(grid);
        System.out.print(primaryScreenBounds.getWidth()/3);//1500
        System.out.print(primaryScreenBounds.getHeight());//1500

    }



}
