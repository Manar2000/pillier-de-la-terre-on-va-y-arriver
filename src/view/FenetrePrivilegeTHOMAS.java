package view;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import model.Player;
import model.PlayersColors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Handler;

public class FenetrePrivilegeTHOMAS extends FenetreDialogue {
    private EventHandler controller;
    private Button valider;
    private ChoiceBox cb;

    public FenetrePrivilegeTHOMAS(Player player, EventHandler controller) throws IOException {
        super("privilège","Vous avez gagner 2 ressources d'un type de votre choix\n Veuillez choisir un type ","Joueur " + player.getColor().getName(), true);
        this.controller=controller;
        setFenTitle("privilège");
        loadForm();
    }

    @Override
    public void loadForm() {
        VBox vbox = new VBox();
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        cb = new ChoiceBox();
        cb.setItems(FXCollections.observableArrayList());
        cb.getItems().addAll("BOIX","PIERRE","SABLE");

        vbox.getChildren().add(cb);
        anchorPane.getChildren().add(vbox);
        Rectangle r = new Rectangle(50,50);
        r.setVisible(false);
        vbox.getChildren().add(r);
        valider = new Button("Valider");
        valider.setOnAction(controller);
        vbox.getChildren().add(valider);
        vbox.setLayoutX(250);
        cb.setValue(0);
    }

    public Button getValider() {
        return valider;
    }

    public ChoiceBox getCb() {
        return cb;
    }
}
