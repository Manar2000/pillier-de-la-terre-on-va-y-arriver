package view.cathedrale;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Parchemin extends Group {

    private ImageView img;
    private int nbPierres;
    private HBox hBox;

    public Parchemin(){
        super();

            Image image = new Image("img/parchemin.png");
            img = new ImageView(image);
            img.setFitHeight(80);
            img.setFitWidth(150);
            this.getChildren().add(img);

        nbPierres=0;
        hBox = new HBox();
        hBox.setLayoutX(17);
        hBox.setLayoutY(40);
        getChildren().add(hBox);
    }
    public void addPierre(){
        nbPierres+=1;
        Group group = new Group();
        Image image ;

            image = new Image("img/pierre.png");
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(15);
            imageView.setFitHeight(18);
            group.getChildren().add(imageView);
            Text t = new Text(Integer.toString(nbPierres));
            t.setFill(Color.WHITE);
            t.setLayoutX(4);
            t.setLayoutY(13);
            group.getChildren().add(t);
            hBox.getChildren().add(group);

    }
}
