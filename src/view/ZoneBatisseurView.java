package view;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import model.PlayersColors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ZoneBatisseurView extends Group {
    PlayersColors colorBatisseur;
    private boolean hasAlreadyBeenChosen; //a deja ete choisie
    private int x,y;
    private int id;
    public ZoneBatisseurView(int x, int y, int id){
        super();
        this.id=id;
        this.x=x; this.y=y;
        Circle circle = new Circle(x,y,20);
        circle.setStyle("-fx-background-color: rgba(255,255,255,0.2)");
        circle.setOpacity(0);
        hasAlreadyBeenChosen=false;
        getChildren().add(circle);
    }

    public PlayersColors getColorBatisseur() {
        return colorBatisseur;
    }

    public void placePion(PlayersColors pc){
        this.colorBatisseur=pc;
        Image image = null;

            String path="img/builder_";
            switch (pc){
                case GREEN:path+="green.png";break;
                case YELLOW:path+="yellow.png";break;
                case RED:path+="red.png";break;
                case BLUE:path+="blue.png";break;
            }
            image = new Image((path));
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(20);imageView.setFitHeight(30);
            imageView.setLayoutX(x-10);imageView.setLayoutY(y-15);
            getChildren().add(imageView);
            hasAlreadyBeenChosen=true;

    }

    public boolean hasAlreadyBeenChosen() {
        return hasAlreadyBeenChosen;
    }

    public int getGroupId() {
        return id;
    }
}
