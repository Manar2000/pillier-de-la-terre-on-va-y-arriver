package view;

import controller.BuilderPlaceController;
import controller.GameController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.*;
import model.ressources.CarteRessources;
import model.ressources.Ressource;
import view.ZoneCliquable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class FenetreCartes extends Stage {
    private ArrayList<Player> lplayers;
    Pane root;
    private Rectangle2D primaryScreenBounds;
    private Text[] texts;
    private String[] s;
    private Text[] playersInfo;
    private Button buttonNext;
    private ImageView[] showMyCards;
    private Button buttonEnd;



    private ImageView[] images;

    public FenetreCartes(ArrayList<Player> lplayers,GameController game) throws IOException {
        super();
        this.lplayers=lplayers;
        game.getFenetreJeu().hide();
        primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        root = FXMLLoader.load(getClass().getResource("fxml/gameScene.fxml"));
        root.getStylesheets().add(getClass().getResource("css/CarteScene.css").toExternalForm());
        setScene(new Scene(root, primaryScreenBounds.getWidth(), primaryScreenBounds.getHeight()- primaryScreenBounds.getHeight()/30));
        setResizable(false);

        texts = new Text[16];
        s = new String[]{"gold","workers","lWorker","master"};
        playersInfo = new Text[lplayers.size()];

        updateTextInfo();
        loadCliquableZone(game);
        show();
        String content1 = "Vous êtes dans la phase de choix des cartes, cliquez sur une carte pour l'acheter,\n " +
                "cliquez sur continuer pour passer la main au prochain joueur \n" +
                "et sur fini pour passer à la phase suivante ";
        new FenetreDialogue("Les Pilliers de la Terre", content1, "tout les joueurs", true);
        String content2 = "joueur "+game.getGame().getCurrentPlayer().getColor().getName()+" c'est votre tour ";
        new FenetreDialogue("Les Pilliers de la Terre", content2, "joueur "+game.getGame().getCurrentPlayer().getColor().getName(), true);
    }



    public Button getButtonNext() {
        return buttonNext;
    }

    public void updateTextInfo() {
        int h =1;
        int w=-5;
        for (int i=0;i<4;i++){
            for (int k=0;k<lplayers.size();k++){
                texts[i+k]=(Text) root.lookup("#"+ s[i]+lplayers.get(k).getColor());
                texts[i+k].setFill(Color.BLACK);
                texts[i+k].setY( primaryScreenBounds.getHeight()/20*h);
                texts[i+k].setX( primaryScreenBounds.getWidth()/1.385+w);
                if (s[i]=="gold") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.GOLD)));
                if (s[i]=="workers") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.SMALLWORKER)));
                if (s[i]=="master") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.MASTERBUILDER)));
            }
            if(i==0)h+=2;
            if(i==1)w+=50;
            if(i==2) {
                w = 45;
            }
        }
        for (int k=0;k<lplayers.size();k++){
            playersInfo[k] = (Text) root.lookup("#" +lplayers.get(k).getColor() + "RessourcesText");
            playersInfo[k].setFill(Color.BLACK);
            playersInfo[k].setX(primaryScreenBounds.getWidth() / 1.9);
            playersInfo[k].setY(primaryScreenBounds.getHeight() / 20);
            playersInfo[k].setText(lplayers.get(k).toString());

        }
    }

    public ImageView[] getShowMyCards() {
        return showMyCards;
    }

    public Rectangle2D getPrimaryScreenBounds() {
        return primaryScreenBounds;
    }

    public void loadCliquableZone(GameController game){
        AnchorPane anchorPane = (AnchorPane) root.lookup("#anchor");
        ArrayList<ImageView> artisans = new ArrayList<>();
        int m=0;
        ImageView imgvide = new ImageView(new Image("img/vide.png"));
        imgvide.setFitWidth(primaryScreenBounds.getWidth()/6.79);imgvide.setFitHeight(primaryScreenBounds.getHeight()/2.15);
        imgvide.setOpacity(1);

        game.getGame().setIndexCurrentPlayer(0);

        images = new ImageView[9];
        int k;
        Random r=new Random();
        ArrayList<ImageView> imgRessources=new ArrayList<>();
        imgRessources.clear();
        for ( ImageView key : game.getGame().getCartesRessources().keySet() ) {
            imgRessources.add(key);
            m++;
        }
        for (int i=0;i<9;i++) {
            images[i] = new ImageView();
            artisans.clear();
            for ( ImageView key : game.getGame().getCartesArtisans().keySet() ) {
                artisans.add(key);
                m++;
            }

            //Collections.shuffle(artisans);
            if (i<2){

                if (artisans.size()==0){
                    images[i]= new ImageView(imgvide.getImage());
                }
                else if(artisans.size()==1){
                    if (i==1)images[i]= new ImageView(imgvide.getImage());
                    else images[i]=artisans.get(i);
                }
                else {
                    images[i]=artisans.get(i);
                }

            }
            else {
                int rnd = new Random().nextInt(imgRessources.size());
                images[i]=imgRessources.get(rnd);
                imgRessources.remove(images[i]);
            }
            images[i].setFitWidth(primaryScreenBounds.getWidth()/6.79);images[i].setFitHeight(primaryScreenBounds.getHeight()/2.15);
            images[i].setOpacity(1);
        }
        int l = 0;
        GridPane grid= new GridPane();

        grid.setMinWidth(0);grid.setMinHeight(0);

        grid.setLayoutX(0);
        grid.setLayoutY(0);
        grid.setStyle("-fx-background-color: rgba(255,255,255,0.4);");

        grid.add(images[0],0, 0);
        grid.add(images[4],1, 0);
        grid.add(images[2],2, 0);
        grid.add(images[3],3, 0);
        grid.add(images[1],0, 1);
        grid.add(images[5],1, 1);
        grid.add(images[6],2, 1);
        grid.add(images[7],3, 1);
        grid.add(images[8],4, 0);

        grid.add(imgvide,4, 1);
        anchorPane.getChildren().add(grid);
        buttonNext = new Button("Continuer");
        buttonNext.setStyle("-fx-border-radius: 10px; -fx-font-family: Arial; -fx-font-size: 24px; -fx-min-width: 200px; -fx-min-height: 70px");
        buttonNext.setLayoutX((primaryScreenBounds.getWidth()/3*1.8));
        buttonNext.setLayoutY(600);

        System.out.print(primaryScreenBounds.getWidth()/3);//1500
        System.out.print(primaryScreenBounds.getHeight());//1500
        buttonEnd = new Button("Fini");
        anchorPane.getChildren().add(buttonNext);
        buttonEnd.setStyle("-fx-border-radius: 10px; -fx-font-family: Arial; -fx-font-size: 24px; -fx-min-width: 200px; -fx-min-height: 70px");
        buttonEnd.setLayoutX((primaryScreenBounds.getWidth()/3*1.8));
        buttonEnd.setLayoutY(500);

        anchorPane.getChildren().add(buttonEnd);

        showMyCards = new ImageView[4];
        double h=2.5;
        for (int i=0;i<4 ;i++){
            showMyCards[i]=new ImageView(new Image("img/mesCartes.png",100,50,false,false));
            showMyCards[i].setPreserveRatio(true);
            showMyCards[i].setX(primaryScreenBounds.getWidth() / 1.15);
            showMyCards[i].setY(primaryScreenBounds.getHeight()/20*h);
            showMyCards[i].setOnMouseClicked(game);
            anchorPane.getChildren().add(showMyCards[i]);
            h+=4.5;
        }

    }
    public ImageView[] getImages() {
        return images;
    }
    public Button getButtonEnd() {
        return buttonEnd;
    }
}
