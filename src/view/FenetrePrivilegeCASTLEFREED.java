package view;

import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import model.Player;

import java.io.IOException;

public class FenetrePrivilegeCASTLEFREED extends FenetreDialogue{
    private Button gold;
    private Button metal;
    public FenetrePrivilegeCASTLEFREED(Player player) throws IOException {
        super("privilège","le château de shiring est libéré!!! \n choisissez soit de gagner une métale ou 6 pièces d'or",
                "player: "+player.getColor().getName(), true);
        setFenTitle("");
        loadForm();
    }
    @Override
    public void loadForm() {
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        gold = new Button("gagner l'or");
        gold.setStyle("-fx-border-radius: 10px; -fx-font-family: Arial; -fx-font-size: 14px; -fx-min-width: 100px; -fx-min-height: 70px");
        metal = new Button("gagner le métal");
        metal.setStyle("-fx-border-radius: 10px; -fx-font-family: Arial; -fx-font-size: 14px; -fx-min-width: 100px; -fx-min-height: 70px");
        anchorPane.getChildren().add(gold);
        anchorPane.getChildren().add(metal);
        gold.setLayoutX(100);
        metal.setLayoutX(300);
    }
    public Button getGoldButton() {
        return gold;
    }
    public Button getMetalButton() {
        return metal;
    }
}
