package view;

import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import model.PlayersColors;

public class ZoneCliquable extends Group {
    private GridPane grid;
    private Text text;
    private boolean isActif;
    private int indiceGrilleCourant;
    private boolean eveche;
    public ZoneCliquable(float x, float y, String label,boolean eveche){
        super();
        this.eveche=eveche;
        setLayoutX(x);setLayoutY(y);
        grid = new GridPane();
        grid.setMinWidth(80);grid.setMinHeight(80);
        text = new Text(label);
        text.setStyle("-fx-font-size: 16px; -fx-font-style: italic;");
        text.setFill(Color.WHITE);
        text.setLayoutY(-5);
        grid.setLayoutX(0);
        grid.setLayoutY(0);
        grid.setStyle("-fx-background-color: rgba(255,255,255,0.4);");
        this.getChildren().add(grid);
        indiceGrilleCourant=0;
        getChildren().add(text);
    }

    public boolean isActif() {
        return isActif;
    }
    public void addPion(PlayersColors color){
        ImageView image = new ImageView() ;
        switch (color){
            case BLUE:
                image.setImage(new Image("img/worker_blue.png"));break;

            case RED:image.setImage(new Image("img/worker_red.png"));break;
            case YELLOW: image.setImage(new Image("img/worker_yellow.png"));break;
            case GREEN: image.setImage(new Image("img/worker_green.png"));break;
        }
        image.setFitWidth(20);image.setFitHeight(20);
        image.setOpacity(1);
        if (eveche)grid.add(image,indiceGrilleCourant%8, indiceGrilleCourant/8);
        else grid.add(image,indiceGrilleCourant%4, indiceGrilleCourant/4);
        indiceGrilleCourant+=1;
    }
    public void vider(){
        grid.getChildren().clear();
    }


    public GridPane getGrid() {
        return grid;
    }

    public void setActif(boolean actif) {
        isActif = actif;
    }

    public void setIndiceGrilleCourant(int indiceGrilleCourant) {
        this.indiceGrilleCourant = indiceGrilleCourant;
    }
}
