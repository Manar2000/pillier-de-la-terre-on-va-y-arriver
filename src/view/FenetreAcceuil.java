package view;

import controller.GameController;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import view.FenetreDialogue;

import java.io.IOException;

public class FenetreAcceuil extends FenetreDialogue {
    private Button play;
    public FenetreAcceuil() throws IOException {
        super("Les Pilliers de la Terre","Bienvenue sur Les Pilliers de La Terre","", false);
        setFenTitle("");
        loadForm();
    }
    @Override
    public void loadForm() {
        AnchorPane anchorPane = (AnchorPane) root.lookup("#form");
        play = new Button("Jouer");
        play.setStyle("-fx-border-radius: 10px; -fx-font-family: Arial; -fx-font-size: 24px; -fx-min-width: 200px; -fx-min-height: 70px");
        anchorPane.getChildren().add(play);
        play.setLayoutX(180);
    }
    public Button getPlay() {
        return play;
    }
    @Override
    public void close(){
        GameController.updateFen();
        super.close();
    }
}
