package view;

import controller.GameController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.ParallelCamera;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Player;
import model.PlayersColors;
import model.ressources.Ressource;
import view.ZoneCliquable;
import view.cathedrale.Parchemin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FenetreJeu extends Stage {


    private ImageView[] showMyCards;
    private ArrayList<Player> lplayers;
    Pane root;
    private Rectangle2D primaryScreenBounds;
    private ZoneCliquable zoneSable;
    private ZoneCliquable zonePierre;
    private ZoneCliquable zoneBois;
    private ZoneCliquable eveche;
    private ZoneCliquable courRoi;
    private Text[] texts;
    private String[] s;
    private Text[] playersInfo;
    private Button buttonNext;
    private Parchemin parchemin;
    private float quotaX;
    private float quotaY;
    private AnchorPane anchorPane;
    private ArrayList<GroupZoneBatisseurs> lGroupeZoneBatisseurs;

    public FenetreJeu(ArrayList<Player> lplayers) throws IOException {
        super();

        this.lplayers=lplayers;
        primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        root = FXMLLoader.load(getClass().getResource("fxml/gameScene.fxml"));
        root.getStylesheets().add(getClass().getResource( "css/gameStyle.css").toExternalForm());
        setScene(new Scene(root, primaryScreenBounds.getWidth(), primaryScreenBounds.getHeight()- primaryScreenBounds.getHeight()/30));
        setResizable(false);

        texts = new Text[16];
        s = new String[]{"gold","workers","lWorker","master"};
        playersInfo = new Text[lplayers.size()];

        AnchorPane anchorPane = (AnchorPane) root.lookup("#anchor");
        buttonNext = new Button("Continuer");
        anchorPane.getChildren().add(buttonNext);
        buttonNext.setLayoutX(0);buttonNext.setLayoutY(0);
        parchemin=new Parchemin();//cathedrale: 439, 444
        parchemin.setLayoutX(479);
        parchemin.setLayoutY(344);
        parchemin.setVisible(true);
        parchemin.addPierre();
        updateTextInfo();

        anchorPane.getChildren().add(parchemin);
        lGroupeZoneBatisseurs= new ArrayList<>();
        System.out.println(primaryScreenBounds.getWidth() + " : " + primaryScreenBounds.getHeight());
        loadCliquableZone();
        showMyCards = new ImageView[4];
        double h=2.5;
        for (int i=0;i<4;i++){
            showMyCards[i]=new ImageView(new Image("img/mesCartes.png",100,50,true,false));
            showMyCards[i].setPreserveRatio(true);
            showMyCards[i].setX(primaryScreenBounds.getWidth() / 1.15);
            showMyCards[i].setY(primaryScreenBounds.getHeight()/20*h);
            anchorPane.getChildren().add(showMyCards[i]);
            h+=4.5;
        }
        loadClickDebug();
        //show();
    }



    public void loadCliquableZone(){
        Rectangle r = new Rectangle(50, 50);
        anchorPane = (AnchorPane) root.lookup("#anchor");
        r.setFill(Color.RED);
        //fen : width, heigth: 1536.0 796.7999877929688
        //zone sable:  x : 244.0 y : 497.0
        //zone pierre :  x : 553.0 y : 542.0
        //zone bois :  x : 955.0 y : 659.0
        //cour du roi (metal):  x : 105.0 y : 650.0
        //eveche : 951.2 y : 224.8
        float quotaX = (float) (primaryScreenBounds.getWidth()/1536);
        float quotaY= (float) (primaryScreenBounds.getWidth()/796.79);
        zoneSable = new ZoneCliquable(244, 497, "sable",false);
        zonePierre = new ZoneCliquable(553, 542, "pierre",false);
        zoneBois = new ZoneCliquable(955, 659, "bois",false);
        courRoi = new ZoneCliquable(105, 650, "cour du roi",false);
        eveche = new ZoneCliquable(951,170, "l'Evéché",true);
        eveche.getGrid().setMinHeight(150);
        eveche.getGrid().setMinWidth(150);
        anchorPane.getChildren().add(eveche);
        anchorPane.getChildren().add(zoneSable);
        anchorPane.getChildren().add(zonePierre);
        anchorPane.getChildren().add(zoneBois);
        anchorPane.getChildren().add(courRoi);
        buttonNext = new Button("Continuer");
        buttonNext.setLayoutX(0); buttonNext.setLayoutY(0);
        anchorPane.getChildren().add(buttonNext);
        Scanner s = new Scanner(System.in);

    }
    public void nextTurn(){
        parchemin.addPierre();
        zoneBois.vider();
        zonePierre.vider();
        zoneSable.vider();
        courRoi.vider();
        eveche.vider();
        clearZoneBatisseurs();

    }
    public Button getButtonNext() {
        return buttonNext;
    }


    public void updateTextInfo() {
        int h =1;
        int w=-5;
        for (int i=0;i<4;i++){
            for (int k=0;k<lplayers.size();k++){
                texts[i+k]=(Text) root.lookup("#"+ s[i]+lplayers.get(k).getColor());
                texts[i+k].setFill(Color.BLACK);
                texts[i+k].setY( primaryScreenBounds.getHeight()/20*h);
                texts[i+k].setX( primaryScreenBounds.getWidth()/1.385+w);
                if (s[i]=="gold") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.GOLD)));
                if (s[i]=="workers") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.SMALLWORKER)));
                if (s[i]=="master") texts[i+k].setText(Integer.toString(lplayers.get(k).getInventory().get(Ressource.MASTERBUILDER)));
            }
            if(i==0)h+=2;
            if(i==1)w+=50;
            if(i==2){w=45;}
        }
        for (int k=0;k<lplayers.size();k++){
            playersInfo[k] = (Text) root.lookup("#" +lplayers.get(k).getColor() + "RessourcesText");
            playersInfo[k].setFill(Color.BLACK);
            playersInfo[k].setX(primaryScreenBounds.getWidth() / 1.9);
            playersInfo[k].setY(primaryScreenBounds.getHeight() / 20);
            playersInfo[k].setText(lplayers.get(k).toString());

        }
    }

    public void loadZoneBatisseurs(){
        double k1 = primaryScreenBounds.getWidth()/1536.;
        double k2 = primaryScreenBounds.getHeight()/824.;
        AnchorPane anchorPane = (AnchorPane) root.lookup("#anchor");
        GroupZoneBatisseurs gzb = new GroupZoneBatisseurs(1);//eveche
        gzb.addZoneBatisseur((int) (904*k1),140);
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);


        gzb=new GroupZoneBatisseurs(2);//kingsbridge
        gzb.addZoneBatisseur((int) (875*k1), (int) (424*k2));
        gzb.addZoneBatisseur((int) (829*k1), (int) (420*k2));
        gzb.addZoneBatisseur((int) (700*k1), (int) (411*k2));
        gzb.addZoneBatisseur((int) (655*k1), (int) (422*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(3);//prieure
        gzb.addZoneBatisseur((int) (1023*k1), (int) (544*k2));
        gzb.addZoneBatisseur((int) (980*k1), (int) (545*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(4);//cour du roi
        gzb.addZoneBatisseur((int) (187*k1), (int) (762*k2));
        gzb.addZoneBatisseur((int) (144*k1), (int) (757*k2));
        gzb.addZoneBatisseur((int) (104*k1), (int) (772*k2));
        gzb.addZoneBatisseur((int) (83*k1), (int) (700*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(5);//shiring
        gzb.addZoneBatisseur((int) (223*k1), (int) (395*k2));
        gzb.addZoneBatisseur((int) (115*k1), (int) (394*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(6);//chateau de shiring
        gzb.addZoneBatisseur((int) (224*k1), (int) (145*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(7);//marche
        gzb.addZoneBatisseur((int) (426*k1), (int) (229*k2));
        gzb.addZoneBatisseur((int) (471*k1), (int) (229*k2));
        gzb.addZoneBatisseur((int) (512*k1), (int) (229*k2));
        gzb.addZoneBatisseur((int) (555*k1), (int) (229*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);

        gzb=new GroupZoneBatisseurs(8);//cathedrale
        gzb.addZoneBatisseur((int) (416*k1), (int) (360*k2));
        anchorPane.getChildren().add(gzb);
        lGroupeZoneBatisseurs.add(gzb);
    }
    public void clearZoneBatisseurs(){
        AnchorPane anchorPane = (AnchorPane) root.lookup("#anchor");
        for(GroupZoneBatisseurs gzb:lGroupeZoneBatisseurs){
            anchorPane.getChildren().remove(gzb);
        }
    }
    private void loadClickDebug() {
        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("x : " + mouseEvent.getX() + "y : " + mouseEvent.getY());
            }
        });
    }

    public ArrayList<GroupZoneBatisseurs> getlGroupeZoneBatisseurs() {
        return lGroupeZoneBatisseurs;
    }

    public ZoneCliquable getZoneSable() {
        return zoneSable;
    }

    public void setZoneSable(ZoneCliquable zoneSable) {
        this.zoneSable = zoneSable;
    }

    public ZoneCliquable getZonePierre() {
        return zonePierre;
    }

    public void setZonePierre(ZoneCliquable zonePierre) {
        this.zonePierre = zonePierre;
    }

    public ZoneCliquable getZoneBois() {
        return zoneBois;
    }

    public void setZoneBois(ZoneCliquable zoneBois) {
        this.zoneBois = zoneBois;
    }

    public ZoneCliquable getCourRoi() {
        return courRoi;
    }

    public void setCourRoi(ZoneCliquable courRoi) {
        this.courRoi = courRoi;
    }
    public ImageView[] getShowMyCards() {
        return showMyCards;
    }
    public ZoneCliquable getEveche() {
        return eveche;
    }

}
