package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import model.Player;
import model.Privilege;
import model.ressources.Ressource;
import view.FenetrePrivilegeCASTLEFREED;
import view.FenetrePrivilegeTHOMAS;

import java.io.IOException;

public class PrivilegeController implements EventHandler {
    private FenetrePrivilegeCASTLEFREED fenetreCASTLEFREED;
    private FenetrePrivilegeTHOMAS fenetrePrivilegeTHOMAS;
    private Player player;
    private Privilege privilege;
    public PrivilegeController(Privilege privilege, Player player) throws IOException {
        this.privilege=privilege;
        this.player=player;
        switch (privilege){
            case CASTLEFREED:
                fenetreCASTLEFREED = new FenetrePrivilegeCASTLEFREED(player);
                fenetreCASTLEFREED.getGoldButton().setOnAction(this);
                fenetreCASTLEFREED.getMetalButton().setOnAction(this);
                break;
            case THOMAS:
                fenetrePrivilegeTHOMAS=new FenetrePrivilegeTHOMAS(player,this);
                break;
            case WILLIAM:
                player.getInventory().put(Ressource.MASTERBUILDERNOIR,1);
                break;
            default:
                player.getPrivileges().replace(privilege,true);

        }
    }
    @Override
    public void handle(Event event) {
        switch (privilege){
            case CASTLEFREED:
                if (fenetreCASTLEFREED.getMetalButton()==event.getTarget()) {
                    player.getInventory().replace(Ressource.METAL,player.getInventory().get(Ressource.METAL)+1);
                }
                else if (fenetreCASTLEFREED.getGoldButton()==event.getTarget()) {
                    player.getInventory().replace(Ressource.GOLD,player.getInventory().get(Ressource.GOLD)+6);
                }

            case THOMAS:
                if(fenetrePrivilegeTHOMAS.getValider()==event.getTarget()){
                    try {
                        String chosenType = (String) fenetrePrivilegeTHOMAS.getCb().getValue();
                        player.getInventory().replace(Ressource.valueOf(chosenType),player.getInventory().get(Ressource.valueOf(chosenType))+2);
                        fenetrePrivilegeTHOMAS.close();

                    }catch (NumberFormatException e){return;}

                }

        }


    }
}
