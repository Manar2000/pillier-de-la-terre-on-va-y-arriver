package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.ImageView;
import model.*;
import model.ressources.Ressource;
import view.*;

import java.io.IOException;
import java.util.*;

public class GameController implements EventHandler {


    private final HashMap<PlayersColors, Player> mapPlayers;
    private Game game;
    private FenetreJeu fenetreJeu;
    private FenetreCartes fenetreCarte;
    private ArrayList<Player> lplayers;
    private Market market;
    private FenetreMarche fenetreMarche;
    public static ArrayList<FenetreDialogue> lFenetre = new ArrayList<>();
    private CartesSceneController carteSceneController;
    private ArrayList<Player> playersInTown;
    private ArrayList<Player> playersOnMarket;

    public ArrayList<Player> getPlayersOnMarket() {
        return playersOnMarket;
    }

    public void setPlayersOnMarket(ArrayList<Player> playersOnMarket) {
        this.playersOnMarket = playersOnMarket;
    }

    public ArrayList<Player> getPlayersInTown() {
        return playersInTown;
    }

    public void setPlayersInTown(ArrayList<Player> playersInTown) {
        this.playersInTown = playersInTown;
    }

    public GameController(ArrayList<Player> lplayers) throws IOException {
        playersInTown=new ArrayList<>();
        game = new Game(lplayers);
        game.initZonesBatisseurs();
        game.initCartesArtisan();
        game.initCartesRessources();
        game.initEvenements(CarteEvenement.values(), game.getEvenements());
        this.lplayers = lplayers;
        int g = 20;
        for (Player p : lplayers) {
            p.getInventory().put(Ressource.GOLD, g);
            g++;
        }

        this.fenetreJeu = new FenetreJeu(lplayers);
        fenetreJeu.getButtonNext().setOnAction(this);
        String content = "Bienvenue dans cette nouvelle partie des Pilliers de la terre vous avez"+game.getNbToursRestants()+" tours pour obtenir le maximum de points de victoire";
        new FenetreDialogue("Les Pilliers de la Terre", content, "Tous les joueurs", true);
        fenetreJeu.getZoneSable().setOnMouseClicked(this);
        fenetreJeu.getCourRoi().setOnMouseClicked(this);
        fenetreJeu.getZoneBois().setOnMouseClicked(this);
        fenetreJeu.getZonePierre().setOnMouseClicked(this);
        playNextTurn(game);
        for (ImageView i : fenetreJeu.getShowMyCards()){
            i.setOnMouseClicked(this);
        }
        mapPlayers= new HashMap<>();
        for (Player p : lplayers){
            mapPlayers.put(p.getColor(),p);
        }
        carteSceneController = new CartesSceneController(lplayers, this);
        this.fenetreCarte = carteSceneController.getFenetreCartes();
    }


    public static void updateFen() {
        if (lFenetre.size() > 0)
            lFenetre.remove(0);
        if (lFenetre.size() > 0)
            lFenetre.get(0).show();
    }

    @Override
    public void handle(Event event) {
        if (event.getSource() == fenetreJeu.getButtonNext()) {
            for (Player p:lplayers){
                if (p.getInventory().get(Ressource.SMALLWORKER)!=0){

                }
            }
            try {
                playNextTurn(game);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (event.getSource() == fenetreCarte.getShowMyCards()[0] || event.getSource() == fenetreJeu.getShowMyCards()[0] ){
            try {
                new FenetreMesCartes(mapPlayers.get(PlayersColors.RED),fenetreCarte.getPrimaryScreenBounds());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (event.getSource() == fenetreCarte.getShowMyCards()[1] || event.getSource() == fenetreJeu.getShowMyCards()[1] ){
            try {
                new FenetreMesCartes(mapPlayers.get(PlayersColors.BLUE),fenetreCarte.getPrimaryScreenBounds());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (event.getSource() == fenetreCarte.getShowMyCards()[2] || event.getSource() == fenetreJeu.getShowMyCards()[2] ){
            try {
                new FenetreMesCartes(mapPlayers.get(PlayersColors.YELLOW),fenetreCarte.getPrimaryScreenBounds());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (event.getSource() == fenetreCarte.getShowMyCards()[3] || event.getSource() == fenetreJeu.getShowMyCards()[3] ){
            try {
                new FenetreMesCartes(mapPlayers.get(PlayersColors.GREEN),fenetreCarte.getPrimaryScreenBounds());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private void playNextTurn(Game game) throws IOException {
        if (game.getNbToursRestants() == 0 ) {
            fenetreJeu.nextTurn();
            game.exchangeAllRessources(game.getlPlayers());
            endGame();

        }
        if (this.game.isMarket()){
            playMarket(playersOnMarket);
            game.setMarket(false);
            game.setTaxes(true);
        }
        else if (this.game.isTaxes()){
            playKingsTax(playersInTown);
            game.setTaxes(false);
        }
        else {
            market = new Market();

            //tour suivant
            if (game.toNextPlayer(true)) {
                game.exchangeAllRessources(game.getlPlayers());
                fenetreJeu.nextTurn();

                carteSceneController = new CartesSceneController(lplayers, this);
                fenetreCarte = carteSceneController.getFenetreCartes();
                String content1="";
                if (game.getNbToursRestants()==0){
                   content1 = "Il reste un seul tour " + " chaque joueurs récupère les ressources sur " +
                            "lesquelles il a placé ses pions pour chaque ouvrier posé";
                }
                else content1 = "Il reste " + Integer.toString(game.getNbToursRestants()) + " tours " + " chaque joueurs récupère les ressources sur " +
                        "lesquelles il a placé ses pions pour chaque ouvrier posé";
                new FenetreDialogue("Les Pilliers de la Terre", content1, "Tous les joueurs", true);

                if (game.getIndexCurrentPlayer() == 0 && game.getNbToursRestants() != 6 && game.getNbToursRestants() != 0) {
                    //privilege count
                    if (game.getCurrentPlayer().getPrivileges().get(Privilege.COUNT)) {
                        new FenetreDialogue("privilège", "le compte Bartholomew vous a donné 2 pièces d'or",
                                "joureur " + game.getCurrentPlayer().getColor().getName(), true);
                        game.getCurrentPlayer().getInventory().replace(Ressource.GOLD, game.getCurrentPlayer().getInventory().get(Ressource.GOLD) + 2);
                    }
                }
            }
            fenetreJeu.updateTextInfo();

        }



    }


    public FenetreCartes getFenetreCarte() {
        return fenetreCarte;
    }

    private void endGame() {

        fenetreJeu.close();
        //new AcceuilController();
        StringBuilder content = new StringBuilder("Bravo au joueur " + game.whoWon(game.getlPlayers()).getColor().getName() + " qui a remporte la partie ! \n" +
                "\n voici le résumé des scores:\n");
        for (Player p : game.getlPlayers()) {
            content.append(p.getColor().getName()).append(" a fait ").append(p.getNumberVictoryPoints()).append(" points \n");
        }
        new FenetreDialogue("Fin du jeu", content.toString(), "Tous les joueurs", true);
    }

    private void addPionTo(PawsZone pawsZone, Player p) {
        int initNumber = pawsZone.getNbPawsPerColor().get(p.getColor());
        pawsZone.getNbPawsPerColor().put(p.getColor(), initNumber + 1);
    }
    public Game getGame() {
        return game;
    }
    /*
    méthode pour faire jouer la taxe du roi
    @param LPlayerInKingCourt: liste des joueurs dont un des bâtisseur
    est dans la cour du roi
     */
    private void playKingsTax(ArrayList<Player> lPlayerInKingCourt) {
        int taxe = game.getKingTax();
        ArrayList<Player> taxedPlayer = new ArrayList<>();
        for (Player p : lplayers) {
            if (!lPlayerInKingCourt.contains(p))
                taxedPlayer.add(p);
        }
        new FenetreImpotRoi(taxe, taxedPlayer, lPlayerInKingCourt);
        game.applyKingTax(lplayers, lPlayerInKingCourt, taxe);
        fenetreJeu.updateTextInfo();
    }

    /*
    methode pour jouer le marché
    @param lplayerInMarket: liste triée des joueurs ayant leur batisseur dans le
    marché
     */
    public void playMarket(ArrayList<Player> lplayersInMarket) {
        if (lplayersInMarket.size()!=0){
            market.initMarket();
            for (Player p : lplayersInMarket)
                market.addExchanger(p);
            fenetreMarche = new FenetreMarche(this);
        }
        else {
            String content1 = "il n'y a aucun batisseur sur le marché cliquer sur continuer";
            new FenetreDialogue("Les Pilliers de la Terre", content1, "Tous les joueurs", true);
        }

    }

    public void sellToMarket(Ressource r, Player p, ChoiceBox cb) {
        try {
            int value = Integer.parseInt((String) cb.getValue());
            market.sell(r, value, p);
            fenetreJeu.updateTextInfo();
            fenetreMarche.play(market.getNextPlayer(), market);
        } catch (NumberFormatException n) {
            ;
        }
    }

    public void passMarketTurn(Player p) {
        market.removeExchanger(p);
        if (market.getlExchangers().size() == 0){
            fenetreMarche.close();
        }
        else
            fenetreMarche.play(market.getNextPlayer(), market);
    }

    public void buyToMarket(Ressource r, Player p, ChoiceBox cb) {
        try {
            int value = Integer.parseInt((String) cb.getValue());
            market.buy(r, value, p);
            fenetreJeu.updateTextInfo();
            fenetreMarche.play(market.getNextPlayer(), market);
        } catch (NumberFormatException n) {
            ;
        }
    }
    public FenetreJeu getFenetreJeu() {
        return fenetreJeu;
    }

    public void startMarket() {
        fenetreMarche.play(market.getNextPlayer(), market);
    }

}