package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.CarteArtisan;
import model.Player;
import model.PlayersColors;
import model.ressources.CarteRessources;
import model.ressources.Ressource;
import view.FenetreCartes;
import view.FenetreDialogue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static model.BonusMalus.PROTEGE_CONTRE_EVENEMENT;

public class CartesSceneController implements EventHandler {
    private ArrayList<Player> players;
    private GameController game;
    private FenetreCartes fenetreCartes;

    public CartesSceneController(ArrayList<Player> p,GameController gc) throws IOException {
        game=gc;
        players=p;
        this.fenetreCartes = new FenetreCartes(players,game);
        for (ImageView img : fenetreCartes.getImages()){
            img.setOnMouseClicked(this);
        }
        fenetreCartes.getButtonEnd().setOnAction(this);
        fenetreCartes.getButtonNext().setOnAction(this);
    }

    public FenetreCartes getFenetreCartes() {
        return fenetreCartes;
    }

    @Override
    public void handle(Event event) {
        if (event.getSource() instanceof ImageView){

            ImageView img = (ImageView)event.getSource();
            HashMap<Ressource,Integer> inventoryCurrentPlayer = game.getGame().getCurrentPlayer().getInventory();


            if (game.getGame().getCartesRessources().get(img)!=null){
                int nbOuvrierAMettre=game.getGame().getCartesRessources().get(img).getOuvriers();
                if (inventoryCurrentPlayer.get(Ressource.SMALLWORKER)>=nbOuvrierAMettre){
                    Ressource ressourceDemandee= game.getGame().getCartesRessources().get(img).getType();
                    PlayersColors colorCurrentPlayer = game.getGame().getCurrentPlayer().getColor();


                    switch (ressourceDemandee){
                        case BOIS:
                            for (int i=0;i<nbOuvrierAMettre;i++){
                                game.getFenetreJeu().getZoneBois().addPion(colorCurrentPlayer);
                                game.getGame().getMap().getZoneBois().getNbPawsPerColor().replace(colorCurrentPlayer,
                                        game.getGame().getMap().getZoneBois().getNbPawsPerColor().get(colorCurrentPlayer)+nbOuvrierAMettre);
                            }
                            break;
                        case SABLE:
                            for (int i=0;i<nbOuvrierAMettre;i++){
                                game.getFenetreJeu().getZoneSable().addPion(colorCurrentPlayer);
                                game.getGame().getMap().getZoneSable().getNbPawsPerColor().replace(colorCurrentPlayer,
                                        game.getGame().getMap().getZoneSable().getNbPawsPerColor().get(colorCurrentPlayer)+nbOuvrierAMettre);
                            }
                            break;
                        case PIERRE:
                            for (int i=0;i<nbOuvrierAMettre;i++){
                                game.getFenetreJeu().getZonePierre().addPion(game.getGame().getCurrentPlayer().getColor());
                                game.getGame().getMap().getZonePierre().getNbPawsPerColor().replace(colorCurrentPlayer,
                                        game.getGame().getMap().getZonePierre().getNbPawsPerColor().get(colorCurrentPlayer)+nbOuvrierAMettre);
                            }
                            break;


                    }
                    game.getFenetreJeu().updateTextInfo();
                    inventoryCurrentPlayer.replace(Ressource.SMALLWORKER, inventoryCurrentPlayer.get(Ressource.SMALLWORKER)- nbOuvrierAMettre);
                    game.getGame().getCurrentPlayer().getCartesRessources().add(game.getGame().getCartesRessources().get(img));
                    img.setImage(new Image("img/carteClose.jpg"));

                }
            }

            CarteArtisan carteArtisan = game.getGame().getCartesArtisans().get(img);

            if(carteArtisan!=null) {
                if (inventoryCurrentPlayer.get(Ressource.GOLD)>= carteArtisan.getCout()){
                    inventoryCurrentPlayer.replace(Ressource.GOLD,inventoryCurrentPlayer.get(Ressource.GOLD)-carteArtisan.getCout());
                    img.setImage(new Image("img/carteClose.jpg"));
                    game.getGame().getCurrentPlayer().getCartes().add(carteArtisan);
                    game.getGame().getCartesArtisans().remove(img);

                }
            }
            fenetreCartes.updateTextInfo();
        }


        if (event.getSource()==fenetreCartes.getButtonNext()){

            game.getGame().setIndexCurrentPlayer(game.getGame().getIndexCurrentPlayer()+1);
            if (game.getGame().getIndexCurrentPlayer()>=game.getGame().getlPlayers().size()){
                game.getGame().setIndexCurrentPlayer(0);
            }
            String content1 = "joueur "+game.getGame().getCurrentPlayer().getColor().getName()+" c'est votre tour ";
            new FenetreDialogue("Les Pilliers de la Terre", content1, "joueur "+game.getGame().getCurrentPlayer().getColor().getName(), true);
        }
        if (event.getSource()==fenetreCartes.getButtonEnd()){
            for (Player p : players){
                if (p.getInventory().get(Ressource.SMALLWORKER)>0) {
                    p.getBonus().replace(PROTEGE_CONTRE_EVENEMENT, true);
                    for (int i = 0; i < p.getInventory().get(Ressource.SMALLWORKER); i++) {
                        game.getFenetreJeu().getEveche().addPion(p.getColor());
                        game.getGame().getMap().getZoneEvache().getNbPawsPerColor().replace(p.getColor(),
                                game.getGame().getMap().getZoneEvache().getNbPawsPerColor().get(p.getColor())+1);
                    }
                    p.getInventory().replace(Ressource.SMALLWORKER, 0);
                }
            }
            game.getFenetreCarte().close();
            game.getFenetreJeu().updateTextInfo();
            game.getFenetreJeu().show();
            Collections.shuffle(game.getGame().getEvenements());
            game.getGame().getEvenements().get(0).play(players);
            System.out.println(game.getGame().getEvenements().get(0));
            String content1 = "évenement! \n  "+game.getGame().getEvenements().get(0).getActionName();
            new FenetreDialogue("Les Pilliers de la Terre", content1, "tous les joueurs ", true);
            game.getFenetreJeu().updateTextInfo();
            game.getGame().getEvenements().remove(0);
            new BuilderPlaceController(game.getFenetreJeu(), players, game, game.getGame());
        }

    }
}
