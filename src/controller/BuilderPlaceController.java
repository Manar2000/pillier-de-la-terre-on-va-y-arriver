package controller;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import model.Game;
import model.Player;
import model.PlayersColors;
import model.ZonesBatisseurs;
import model.ressources.Ressource;
import view.*;

import java.util.ArrayList;
import java.util.Collections;

import static model.BonusMalus.DEUX_BATISSEURS_TOUR_SUIVANT;
import static model.BonusMalus.PROTEGE_CONTRE_EVENEMENT;

public class BuilderPlaceController implements EventHandler {
    private FenetreJeu fenetreJeu;
    private ArrayList<PlayersColors> lBuilder;
    private GameController gameController;
    private PlayersColors currentPlayer;
    private ArrayList<Player> lPlayers;
    private Game gameModel;
    private int initlBuilderSize;
    private FenetrePayToPlaceBuilder fen;
    private ArrayList<Player> playersOnMarket;
    private ArrayList<Player> playersInTown;
    private int somme;

    public ArrayList<Player> getPlayersInTown() {
        return playersInTown;
    }

    public BuilderPlaceController(FenetreJeu fenetreJeu, ArrayList<Player> lPlayers, GameController gameController, Game gameModel) {
        playersInTown=new ArrayList<>();
        playersOnMarket=new ArrayList<>();
        this.fenetreJeu = fenetreJeu;
        this.gameController = gameController;
        this.gameModel = gameModel;
        lBuilder = new ArrayList<>();
        this.lPlayers=lPlayers;
        for (Player p : lPlayers) {
            PlayersColors pc=p.getColor();
            p.getInventory().put(Ressource.MASTERBUILDER,3);
            lBuilder.add(pc);
            lBuilder.add(pc);
            lBuilder.add(pc);
        }
        initlBuilderSize=lBuilder.size();
        Collections.shuffle(lBuilder);
        fenetreJeu.loadZoneBatisseurs();
        askToPlaceOneBuilder();
    }
    public void askToPlaceOneBuilder(){
        PlayersColors builderColor = lBuilder.remove(0);
        currentPlayer = builderColor;
        Player p = findPlayer();
        somme = 0;
        if(lBuilder.size()-initlBuilderSize<7)
            somme =8-(initlBuilderSize-lBuilder.size());
        fen = new FenetrePayToPlaceBuilder(somme, p);
        if(fen.getPay()!=null)fen.getPay().setOnAction(this);
        if(fen.getNotPay()!=null)fen.getNotPay().setOnAction(this);
        fen.setOnCloseRequest(windowEvent -> {
            fen.close();
            if (lBuilder.size() > 0)
                askToPlaceOneBuilder();

        });
    }

    private Player findPlayer() {
        Player p = null;
        for(Player p1 : lPlayers){
            if(p1.getColor().getName().equals(currentPlayer.getName())) {
                p=p1;
                break;
            }
        }
        return p;
    }

    public void placeOneBuilder(){
        if(lBuilder.size()>0) {
            for (GroupZoneBatisseurs gzb : fenetreJeu.getlGroupeZoneBatisseurs()) {
                for (ZoneBatisseurView zb : gzb.getlZonesBatisseurs()) {
                    zb.setOnMouseClicked(this);
                }
            }
        }
    }
    @Override
    public void handle(Event event) {
        if(currentPlayer!=null && event.getSource() instanceof ZoneBatisseurView) {
            ZoneBatisseurView zb = (ZoneBatisseurView) event.getSource();
            ZonesBatisseurs modelZone = ZonesBatisseurs.find(zb.getGroupId());
            if (!zb.hasAlreadyBeenChosen() && modelZone!=null && modelZone.addBatisseur(currentPlayer)) {
                zb.placePion(currentPlayer);
                Player p = findPlayer();
                int n= p.getInventory().get(Ressource.MASTERBUILDER);
                p.getInventory().put(Ressource.MASTERBUILDER,n-1);
                if (zb.getGroupId()==7){
                    playersOnMarket.add(p);
                    gameModel.setMarket(true);
                }
                if (zb.getGroupId()==4){
                    playersInTown.add(p);

                }
                if (zb.getGroupId()==1){
                    p.getBonus().replace(PROTEGE_CONTRE_EVENEMENT,true);
                }
                if (zb.getGroupId()==2){
                    p.getBonus().replace(DEUX_BATISSEURS_TOUR_SUIVANT,true);
                }

                fenetreJeu.updateTextInfo();
                if (lBuilder.size() > 0)
                    askToPlaceOneBuilder();
                else {
                    currentPlayer=null;
                }
            }
        }
        else{
            fen.close();
            if(event.getSource()==fen.getPay()){
                Player p = findPlayer();
                int n = p.getInventory().get(Ressource.GOLD);
                p.getInventory().put(Ressource.GOLD, n-somme);
                fenetreJeu.updateTextInfo();
                placeOneBuilder();
            }
            else{
                if (lBuilder.size() > 0)
                    askToPlaceOneBuilder();
                else {
                    gameController.getGame().setIndexCurrentPlayer(0);
                    gameController.setPlayersInTown(playersInTown);
                    gameController.setPlayersOnMarket(playersOnMarket);
                    //gameModel.setMarket(true);

                }
            }
        }
    }
}
