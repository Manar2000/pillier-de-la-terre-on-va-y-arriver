import model.Map;
import model.PawsZone;
import model.PlayersColors;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

public class MapUnitTest {
    @Test
    public void testMapConstructor(){
        Map map = new Map();
        String[] tabName={"zone sable","zone bois", "zone pierre", "cour du roi","évéché"};
        testPawsZone(map.getZoneSable(), tabName[0]);
        testPawsZone(map.getZoneBois(), tabName[1]);
        testPawsZone(map.getZonePierre(), tabName[2]);
        testPawsZone(map.getCourRoi(), tabName[3]);
        testPawsZone(map.getZoneEvache(), tabName[4]);
    }
    public void testPawsZone(PawsZone pawsZone, String name){
        Assert.assertEquals(pawsZone.getName(), name);
        for(PlayersColors color : PlayersColors.values()){
            int nb = pawsZone.getNbPawsPerColor().get(color);
            Assert.assertEquals(nb,0);
        }
    }
}
