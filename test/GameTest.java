import model.*;
import model.ressources.Ressource;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;

public class GameTest {
    @Test
    public void testInitEvenements(){
        ArrayList<Player> lPlayer = Mockito.mock(ArrayList.class);
        ArrayList<CarteEvenement> lEvenements = Mockito.mock(ArrayList.class);
        Game game = new Game(lPlayer);
        game.initEvenements(CarteEvenement.values(), lEvenements);
        for(CarteEvenement e : CarteEvenement.values()) {
            Mockito.verify(lEvenements).add(e);
        }
    }
    @Test
    public void testRefreshZonesBatisseurs(){
        ArrayList<ZonesBatisseurs> lZonesBatisseurs = Mockito.mock(ArrayList.class);
        Mockito.when(lZonesBatisseurs.size()).thenReturn(3);
        Mockito.when(lZonesBatisseurs.get(Mockito.anyInt())).thenReturn(ZonesBatisseurs.CARRIERE);
        Game game = new Game(Mockito.mock(ArrayList.class));
        game.refreshZonesBatisseurs(lZonesBatisseurs);
        Mockito.verify(lZonesBatisseurs, Mockito.times(3)).get(Mockito.anyInt());
    }
    @Test
    public void testWhoWon(){
        ArrayList<Player> lPlayer = new ArrayList<>();
        Player p1 = Mockito.mock(Player.class);
        Mockito.when(p1.getNumberVictoryPoints()).thenReturn(0);
        Player p2 = Mockito.mock(Player.class);
        Mockito.when(p2.getNumberVictoryPoints()).thenReturn(10);
        Player p3 = Mockito.mock(Player.class);
        Mockito.when(p3.getNumberVictoryPoints()).thenReturn(4);
        lPlayer.add(p1);
        lPlayer.add(p2);
        lPlayer.add(p3);
        Game game = new Game(Mockito.mock(ArrayList.class));
        Player winner = game.whoWon(lPlayer);
        Assert.assertEquals(winner, p2);
    }
    @Test
    public void toNextPlayerNotEndOfTurn(){
        ArrayList<Player> lPlayers = Mockito.mock(ArrayList.class);
        Mockito.when(lPlayers.size()).thenReturn(10);
        Game game = new Game(lPlayers);
        game.setIndexCurrentPlayer(6);
        boolean b = game.toNextPlayer(false);
        Assert.assertFalse(b);
    }
    @Test
    public void toNextPlayerEndOfTurn(){
        ArrayList<Player> lPlayers = Mockito.mock(ArrayList.class);
        Mockito.when(lPlayers.size()).thenReturn(10);
        Game game = new Game(lPlayers);
        game.setIndexCurrentPlayer(9);
        boolean b = game.toNextPlayer(false);
        Assert.assertTrue(b);
    }
    @Test
    public void testToNextTurn(){
        ArrayList<Player> lPlayer = new ArrayList<>();
        Game game = new Game(lPlayer);
        lPlayer.add(new Player(PlayersColors.RED));
        lPlayer.add(new Player(PlayersColors.BLUE));
        HashMap<BonusMalus, Boolean> mBonus1 = Mockito.mock(HashMap.class);
        HashMap<BonusMalus, Boolean> mBonus2 = Mockito.mock(HashMap.class);
        Mockito.when(mBonus1.get(BonusMalus.PERTE_UNE_RESSOURCE_PAR_CARTE)).thenReturn(true);
        Mockito.when(mBonus1.get(BonusMalus.RESSOURCE_EN_PLUS_PAR_CARTE)).thenReturn(false);
        Mockito.when(mBonus2.get(BonusMalus.RESSOURCE_EN_PLUS_PAR_CARTE)).thenReturn(true);
        Mockito.when(mBonus2.get(BonusMalus.PERTE_UNE_RESSOURCE_PAR_CARTE)).thenReturn(false);
        for(BonusMalus bonus : BonusMalus.values()){
            if(bonus!=BonusMalus.PERTE_UNE_RESSOURCE_PAR_CARTE
                    && bonus!=BonusMalus.RESSOURCE_EN_PLUS_PAR_CARTE){
                Mockito.when(mBonus1.get(bonus)).thenReturn(false);
                Mockito.when(mBonus2.get(bonus)).thenReturn(false);
            }
        }


        lPlayer.get(0).setBonus(mBonus1);
        lPlayer.get(1).setBonus(mBonus2);
        HashMap<Ressource, Integer> mRessources1 = Mockito.mock(HashMap.class);
        HashMap<Ressource, Integer> mRessources2 = Mockito.mock(HashMap.class);
        Mockito.when(mRessources1.get(Mockito.anyObject())).thenReturn(10);
        Mockito.when(mRessources2.get(Mockito.anyObject())).thenReturn(10);
        lPlayer.get(0).setInventory(mRessources1);
        lPlayer.get(1).setInventory(mRessources2);
        game.toNextTurn(lPlayer, false);
        Mockito.verify(mRessources1).put(Mockito.anyObject(), Mockito.eq(10));
        Mockito.verify(mRessources2).put(Mockito.anyObject(), Mockito.eq(10));
    }
    @Test(expected = Exception.class)
    public void testInitCartesRessources(){
        Game game = new Game(Mockito.mock(ArrayList.class));
        game.initCartesRessources();
    }
    @Test
    public void testExchangeAllRessources(){
        Player p = Mockito.mock(Player.class);

    }
}
