import exception.NegativeQuantityException;
import model.Market;
import model.Player;
import model.PlayersColors;
import model.ressources.Ressource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;

public class MarketUnitTest {

    @Test
    public void testBuyAnNoneExchangeableResource() {

        Player p = new Player(PlayersColors.BLUE);
        Market m = new Market();
        boolean b = m.buy(Ressource.GOLD,10,p);
        Assert.assertFalse(b);
    }

    @Test
    public void testBuyMetal() {

        Player p = new Player(PlayersColors.BLUE);
        Market m = new Market();
        boolean b = m.buy(Ressource.METAL,10,p);
        Assert.assertFalse(b);
    }

    @Test
    public void testBuyAnExchangeableResourceWithoutEnoughMoney() {

        Player p = new Player(PlayersColors.BLUE);
        Market m = new Market();
        boolean b = m.buy(Ressource.BOIS,100,p);
        Assert.assertFalse(b);
    }

    @Test
    public void testBuyAnExchangeableResourceWithEnoughMoney() {
        Player p = Mockito.mock(Player.class);
        HashMap<Ressource, Integer> inventory = Mockito.mock(HashMap.class);
        Mockito.when(p.pay(Mockito.anyInt(),Mockito.anyBoolean())).thenReturn(true);
        Mockito.when(p.getInventory()).thenReturn(inventory);
        Mockito.when(inventory.get(Mockito.anyObject())).thenReturn(0);
        Market m = new Market();
        boolean b = m.buy(Ressource.BOIS,1,p);
        Mockito.verify(inventory).put(Ressource.BOIS,1);
        Assert.assertTrue(b);
    }

    @Test
    public void testSellAnNoneExchangeableResource() {

        Player p = new Player(PlayersColors.BLUE);
        Market m = new Market();
        boolean b = m.sell(Ressource.GOLD,10,p);
        Assert.assertFalse(b);
    }


    @Test
    public void testSellAnExchangeableResourceWithoutEnoughResource() {

        Player p = new Player(PlayersColors.BLUE);
        Market m = new Market();
        boolean b = m.sell(Ressource.BOIS,100,p);
        Assert.assertFalse(b);
    }

    @Test
    public void testSellAnExchangeableResourceWithEnoughResource() {
        Player p = Mockito.mock(Player.class);
        HashMap<Ressource, Integer> inventory = Mockito.mock(HashMap.class);
        Mockito.when(p.pay(Mockito.anyInt(),Mockito.anyBoolean())).thenReturn(true);
        Mockito.when(p.getInventory()).thenReturn(inventory);
        Mockito.when(inventory.get(Ressource.METAL)).thenReturn(1);
        Mockito.when(inventory.get(Ressource.GOLD)).thenReturn(0);
        Market m = new Market();
        boolean b = m.sell(Ressource.METAL,1,p);
        Mockito.verify(inventory).put(Ressource.GOLD,Ressource.METAL.getValue());
        Assert.assertTrue(b);
    }
}
