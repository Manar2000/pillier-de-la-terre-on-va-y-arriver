import exception.NegativeQuantityException;
import model.BonusMalus;
import model.Player;
import model.PlayersColors;
import model.Privilege;
import model.ressources.Ressource;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.HashMap;

public class PlayerUnitTest {
    @Test
    public void testRefreshBonus(){
        Player p = new Player(PlayersColors.BLUE);
        HashMap<BonusMalus, Boolean> mBonus = Mockito.mock(HashMap.class);
        p.setBonus(mBonus);
        p.refreshBonus();
        for(BonusMalus bonus: BonusMalus.values()) {
            Mockito.verify(mBonus).put(bonus, false);
        }
    }
    @Test
    public void testSetupInventory(){
        Player p = new Player(PlayersColors.GREEN);
        HashMap<Ressource, Integer> mRessources = Mockito.mock(HashMap.class);
        p.setInventory(mRessources);
        p.setupInventory();
       Mockito.verify(mRessources).put(Ressource.BOIS, 0);
        Mockito.verify(mRessources).put(Ressource.METAL, 0);
        Mockito.verify(mRessources).put(Ressource.PIERRE, 0);
        Mockito.verify(mRessources).put(Ressource.SABLE, 0);
        Mockito.verify(mRessources).put(Ressource.MASTERBUILDER, 3);
        Mockito.verify(mRessources).put(Ressource.SMALLWORKER, 12);
    }
    @Test
    public void testSetupPrivileges(){
        Player p = new Player(PlayersColors.RED);
        HashMap<Privilege, Boolean> mPrivileges = Mockito.mock(HashMap.class);
        p.setPrivileges(mPrivileges);
        p.setupPrivileges();
        for(Privilege privilege: Privilege.values()){
            Mockito.verify(mPrivileges).put(privilege, false);
        }
    }
    @Rule
    public ExpectedException ecouteur = ExpectedException.none();

    @Test(expected = NegativeQuantityException.class)
    public void testAddToInventryNegative() throws NegativeQuantityException {

        Player p = new Player(PlayersColors.BLUE);
        p.addToInventory(Ressource.METAL,-2);
    }

    @Test
    public void testAddToInventory() throws NegativeQuantityException {

        Player p = new Player(PlayersColors.BLUE);
        p.addToInventory(Ressource.METAL,2);
        int q = p.getInventory().get(Ressource.METAL);
        Assert.assertEquals(2,q);
    }

    @Test
    public void testAddToNumberVictory() {

        Player p = new Player(PlayersColors.BLUE);
        p.addToNumberVictory(10);
        int points = p.getNumberVictoryPoints();
        Assert.assertEquals(10,points);
    }

    @Test
    public void testPayWithEnoughMoneyAndForcePayFalse() {
        Player p = new Player(PlayersColors.BLUE);
        HashMap<Ressource, Integer> inventory = Mockito.mock(HashMap.class);
        p.setInventory(inventory);
        Mockito.when(inventory.get(Ressource.GOLD)).thenReturn(10);
        boolean b = p.pay(1,false);
        Assert.assertTrue(b);
    }

    @Test
    public void testPayWithoutEnoughMoneyAndForcePayFalse() {
        Player p = new Player(PlayersColors.BLUE);
        HashMap<Ressource, Integer> inventory = Mockito.mock(HashMap.class);
        p.setInventory(inventory);
        Mockito.when(inventory.get(Ressource.GOLD)).thenReturn(50);
        boolean b = p.pay(10000,false);
        Assert.assertFalse(b);
    }

    @Test
    public void testPayWithoutEnoughMoneyAndForcePayTrue() {
        Player p = new Player(PlayersColors.BLUE);
        HashMap<Ressource, Integer> inventory = Mockito.mock(HashMap.class);
        p.setInventory(inventory);
        Mockito.when(inventory.get(Ressource.GOLD)).thenReturn(200);
        boolean b = p.pay(10000,true);
        Mockito.verify(inventory).put(Ressource.GOLD, 0);
        Assert.assertTrue(b);
    }
}
